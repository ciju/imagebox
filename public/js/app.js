/*global jQuery, _*/
(function (G, $, _) {
    var images = [],
        current_idx = 0,
        current_jcrop = null;

    $.get('/img', function (res) {
        // FIX check for err
        images = res;
        showImg(current_idx);
        _hookupContorls();
    });

    function _hookupContorls() {
        $('a').on('click', function() {
            var fwd = $(this).hasClass('j_prev') ? -1 : 1;
            current_idx += fwd;

            console.log('click happened', fwd, current_idx);
            current_jcrop.destroy();

            showImg(current_idx);
        });

    }

    function _showControls(idx) {
        $('a').show();
        if (idx === images.length-1) {
            $('.j_next').hide();
        }
        if (idx === 0) {
            $('.j_prev').hide();
        }
    }


    function showImg(idx) {
        var img = images[idx];
        console.log('setting', idx, img.name);
        $('#image')
            .html('')
            .append('<img src="'+img.name+'" />');

        var cord = [img.x, img.y, img.x2, img.y2];

        _showControls(idx);

        console.log('cord', cord, $('#image img').attr('src'));

        $('#image img').Jcrop({
            setSelect: cord,
            onSelect: saveSelect,
            onChange: function () {
                console.log('change');
            }
        }, function () {
            current_jcrop = this;
        });

        function saveSelect(c) {
            console.log('save ', c);
            img.x = c.x;
            img.y = c.y;
            img.x2 = c.x2;
            img.y2 = c.y2;

            $.ajax({
                url: '/img/'+img.sha,
                type: 'POST',
                data: JSON.stringify(c),
                dataType: 'json',
                contentType: 'application/json'
            }, function (res) {
                console.log('done', res);
            });
        }
    }

})(window, jQuery, _);
