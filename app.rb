# -*- coding: utf-8 -*-
require 'json'
require 'sinatra'
require 'sinatra/json'
require 'mongo'
require 'haml'
require 'digest'

require 'pry'

include Mongo

Db = Mongo::Connection.new.db("imgbox", :pool_size => 5, :timeout => 5)


class ImageTag < Sinatra::Base
  helpers Sinatra::JSON

  helpers do
    # a helper method to turn a string ID
    # representation into a BSON::ObjectId
    def objectid val
      BSON::ObjectId.from_string(val)
    end

    def document_by_sha id
      doc = Db.collection('imgcol').find_one({sha: id})
      return nil if !doc
      doc
    end
  end

  set :static, true
  set :public_folder, File.dirname(__FILE__) + '/public'
  set :json_encoder, :to_json

  # get images with a JSON call for images. Pick each one and show
  # current region.
  get '/' do
    haml :index, :attr_wrapper => '"', :locals => {:title => 'hello'}
  end

  get '/img' do
    fls = Dir['./public/images/*.jpg']

    fls = fls.map {|f|
      id = Digest::SHA2.file(f).hexdigest
      doc = document_by_sha id

      if doc.nil?
        doc = {
          # _id: object_id(id),
          sha: id,
          x: 20,
          y: 20,
          x2: 0,
          y2: 0
        }
        Db.collection('imgcol').insert(doc)
        puts "inserted one #{f} #{id}"
      else
        puts "found one, #{id}"
      end

      {
        name: f.sub('./public', '')
      }.merge(doc)
    }

    json fls
  end

  post '/img/:id' do |id|
    doc = document_by_sha id
    query = {sha: id}

    c = JSON.parse(request.body.read.to_s)
    update = {
      sha: id,
      x: c['x'],
      y: c['y'],
      x2: c['x2'],
      y2: c['y2']
    }

    puts "saving #{query.to_json}"

    doc = Db.collection('imgcol')
      .find_and_modify(:query => query, :update => update)

    puts "saved #{doc.to_json}"

    json doc
  end

  get '/img/:id/selection' do |img|
  end
  post '/img/:id/selection' do |img|
  end

end

ImageTag.run!
